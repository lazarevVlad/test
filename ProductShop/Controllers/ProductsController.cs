﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ProductShop.Models;

namespace ProductShop.Controllers
{
    public class ProductsController : Controller
    {
        private ProductShopContext db = new ProductShopContext();

        // GET: Products
        public ActionResult Index()
        {
            return View(db.Products.ToList());
        }
        [HttpGet]
        public ActionResult EditProduct(int? id = null)
        {
            if (id == null)
            {
                return View();
            }
            return View(db.Products.Find(id));
        }
        [HttpPost]
        public ActionResult EditProduct(Product product)
        {
            if (product.ProductId == null)
            {
                db.Entry(product).State = EntityState.Added;
            }
            else
            {
                db.Entry(product).State = EntityState.Modified;
            }
            db.SaveChanges();
            return RedirectToAction("Index");
        }




        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}
