﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ProductShop.Models;

namespace ProductShop.Controllers
{
    public class OrdersController : Controller
    {
        ProductShopContext db = new ProductShopContext();
        // GET: Orders
        public ActionResult Index()
        {
            var order = db.Orders.Include(a => a.Product).Include(a => a.User);
            return View(order.ToList());
        }


        [HttpGet]
        public ActionResult EditOrder(int? id = null)
        {
            OrderViewModel orderViewModel = new OrderViewModel()
            {
                ProductList = new SelectList(db.Products.ToList(), "ProductId", "Description"),
                UserList = new SelectList(db.Users.ToList(), "UserId", "SecondName")
            };
            if (id == null)
            {
                return View(orderViewModel);
            }
            orderViewModel.Order = db.Orders.Find(id);
            return View(orderViewModel);
        }
        [HttpPost]
        public ActionResult EditOrder(Order order)
        {
            if (order.OrderId == null)
            {
                order.OrderCreateDate =  DateTime.Now;
                db.Entry(order).State = EntityState.Added;
            }
            else
            {
                order.OrderCreateDate = DateTime.Now;
                db.Entry(order).State = EntityState.Modified;
            }
            db.SaveChanges();
            return RedirectToAction("Index");
        }



        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}