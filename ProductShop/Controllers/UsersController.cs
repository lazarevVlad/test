﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ProductShop.Models;

namespace ProductShop.Controllers
{
    public class UsersController : Controller
    {
        ProductShopContext db = new ProductShopContext();
        // GET: User
        public ActionResult Index()
        {
            return View(db.Users.ToList());
        }

        public ActionResult UserOrders()
        {
            return View("Index");
        }

        [HttpGet]
        public ActionResult EditUser(int? id = null)
        {
            if (id == null)
            {
                return View();
            }
            return View(db.Users.Find(id));
        }
        [HttpPost]
        public ActionResult EditUser(User user)
        {
            if (user.UserId == null)
            {
                db.Entry(user).State = EntityState.Added;
            }
            else
            {
                db.Entry(user).State = EntityState.Modified;
            }
            db.SaveChanges();
            return RedirectToAction("Index");
        }


        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}