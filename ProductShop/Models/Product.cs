﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ProductShop.Models
{
    public class Product
    {

        public int? ProductId { get; set; }
        [Display(Name = "цена")]
        public decimal Price { get; set; }
        [Display(Name = "описание")]
        public string Description { get; set; }
        [Display(Name = "дата изготовления")]
        public DateTime ProductCreateDate { get; set; }
    }
}