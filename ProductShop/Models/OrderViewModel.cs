﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProductShop.Models
{
    public class OrderViewModel
    {
        public SelectList ProductList { get; set; }
        public SelectList UserList { get; set; }
        public Order Order { get; set; }
    }
}