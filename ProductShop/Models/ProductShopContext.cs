﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace ProductShop.Models
{
    public class ProductShopContext: DbContext
    {
        public ProductShopContext() : base("ProductShopConnectionString")
        {           
        }

        public DbSet<Product> Products { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Order> Orders { get; set; }

    }
}