﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace ProductShop.Models
{
    public class ProductShopDBinitializer : DropCreateDatabaseAlways<ProductShopContext>
    {
        protected override void Seed(ProductShopContext context)
        {
            context.Products.Add(new Product() { ProductId = 1, Description = "Cheese", Price = 5000, ProductCreateDate = DateTime.Now });
            context.Products.Add(new Product() { ProductId = 2, Description = "Milk", Price = 4000, ProductCreateDate = DateTime.Now });
            context.Products.Add(new Product() { ProductId = 3, Description = "Icecream", Price = 6000, ProductCreateDate = DateTime.Now });

            context.Users.Add(new User() {  FirstName = "Ivan", SecondName = "Ivanov", RegistrationDate = DateTime.Now, Email = "qew@qwe.by"});
            context.Users.Add(new User() {  FirstName = "Petr", SecondName = "Petrov", RegistrationDate = DateTime.Now, Email = "ewq@ewq.by"});

            //context.Orders.Add(new Order() {OrderCreateDate = DateTime.Now, User = context.Users.Find(1), UserId  = 1, OrderId = 1, Product = context.Products.Find(1), ProductCount = 4, ProductId = 1});
            //context.Orders.Add(new Order() {OrderCreateDate = DateTime.Now, User = context.Users.Find(2), UserId  = 2, OrderId = 2, Product = context.Products.Find(2), ProductCount = 2, ProductId = 2});

            base.Seed(context);
        }
    }
}