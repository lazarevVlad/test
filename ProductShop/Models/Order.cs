﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ProductShop.Models
{
    public class Order
    {
        public int? OrderId { get; set; }
        [Display(Name = "Product")]
        public int ProductId { get; set; }
        [Display(Name = "User name")]
        public int UserId { get; set; }
        [Display(Name = "Count product")]
        public int ProductCount { get; set; }
        public DateTime OrderCreateDate { get; set; }
        public Product Product { get; set; }
        public User User { get; set; }
    }
}