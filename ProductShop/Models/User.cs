﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProductShop.Models
{
    public class User
    {
        public int? UserId { get; set; }
        public string FirstName { get; set; }
        public string SecondName { get; set; }
        public DateTime RegistrationDate { get; set; }
        public string Email { get; set; }
    }
}